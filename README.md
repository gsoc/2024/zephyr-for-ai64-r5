Zephyr Support for R5 on BeagleBone-AI64
========================================

Introduction:
-------------

This project currently attempts to enable Zephyr-OS support on BeagleBone-AI64

All work is being performed in upstream directly. This project is present purely for planning purposes and issue tracking will be used to track various Pull requests and development states.

Current Status:
---------------

https://forum.beagleboard.org/t/weekly-progress-report-thread-upstreaming-zephyr-support-on-beaglebone-ai-64/

Project Tracking:
-----------------

https://openbeagle.org/gsoc/2024/zephyr-for-ai64-r5/-/boards

See issues tracking pull requests, development tasks, who is assigned etc.

Collaboration:
--------------

Join the discussion on BeagleBoard.org GSoC Channel and discord:

https://discord.com/channels/1108795636956024986/1239199489827541074

See https://gsoc.beagleboard.org/ for additional information including when we have our weekly meetings (Usually Thursdays)

Folks are welcome to join us and even listen in from time to time on discord.

Development methodology:
------------------------

This project shall work directly in upstream. all pull requests for drivers shall involve:

* Documentation
* Test
* Driver code
